/*
Copyright 2011. All rights reserved.
Institute of Measurement and Control Systems
Karlsruhe Institute of Technology, Germany

This file is part of libelas.
Authors: Andreas Geiger

libelas is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 3 of the License, or any later version.

libelas is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
libelas; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301, USA
*/

// Demo program showing how libelas can be used, try "./elas -h" for help
// elas C:\Users\Kyle\Documents\School\2017-spring\computer-vision\project\pgm-files\ C:\Users\Kyle\Documents\School\2017-spring\computer-vision\project\pgm-files\ C:\Users\Kyle\Documents\School\2017-spring\computer-vision\project\png-files\ C:\Users\Kyle\Documents\School\2017-spring\computer-vision\project\png-files\

#include "elas.h"
#include "image.h"
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;

namespace patch {
    template <typename T> string to_string(const T& n) {
        ostringstream stm;
        stm << n;
        return stm.str();
    }
}

// compute disparities of pgm image input pair file_1, file_2
void process(const char* left_pgm_path, const char* right_pgm_path, const char* left_png_path, const char* right_png_path) {
	double f = 670.0153;
	double cx = 618.6082;
	double cy = 349.9153;
	double baseline = 0.12;

	for (int i = 0; i < 7800; i++) {
		string pad = "00000";
		if (i > 999) {
			pad = "00";
		}
		else if (i > 99) {
			pad = "000";
		}
		else if (i > 9) {
			pad = "0000";
		}

		string left_pgm(left_pgm_path);
		string right_pgm(right_pgm_path);
		string left_png(left_png_path);
		string right_png(right_png_path);
		left_pgm = left_pgm + "I1_" + pad + patch::to_string(i) + ".pgm";
		right_pgm = right_pgm + "I2_" + pad + patch::to_string(i) + ".pgm";
		left_png = left_png + "I1_" + pad + patch::to_string(i) + ".png";
		right_png = right_png + "I2_" + pad + patch::to_string(i) + ".png";

		// load images
		image<uchar> *I1, *I2;
		I1 = loadPGM(left_pgm.c_str());
		I2 = loadPGM(right_pgm.c_str());

		// check for correct size
		if (I1->width() <= 0 || I1->height() <= 0 || I2->width() <= 0 || I2->height() <= 0 ||
			I1->width() != I2->width() || I1->height() != I2->height()) {
			cout << "ERROR: Images must be of same size, but" << endl;
			cout << "       I1: " << I1->width() << " x " << I1->height() <<
				", I2: " << I2->width() << " x " << I2->height() << endl;
			delete I1;
			delete I2;
			return;
		}

		// get image width and height
		int32_t width = I1->width();
		int32_t height = I1->height();

		// allocate memory for disparity images
		const int32_t dims[3] = { width,height,width }; // bytes per line = width
		float* D1_data = (float*)malloc(width*height * sizeof(float));
		float* D2_data = (float*)malloc(width*height * sizeof(float));

		// process
		Elas::parameters param;
		param.postprocess_only_left = false;
		Elas elas(param);
		elas.process(I1->data, I2->data, D1_data, D2_data, dims);

		// ----------------------------------------------------------- //
		// -------------------- Compute 3D points -------------------- //
		// ----------------------------------------------------------- //
		ofstream project;
		string filename = "project_" + patch::to_string(i) + ".txt";
		project.open(filename.c_str());
		cv::Mat imgRGB = cv::imread(left_png);

		for (int32_t i = 0; i < width*height; i++) {
			double d = D1_data[i];
			if (d > 8) {
				int x = i % width;
				int y = i / width;
				project << (x - cx) * baseline / d << " ";
				project << (y - cy) * baseline / d << " ";
				project << f * baseline / d << " ";

				cv::Vec3b colors = imgRGB.at<cv::Vec3b>(y, x);
				project << patch::to_string(colors.val[2]) << " ";
				project << patch::to_string(colors.val[1]) << " ";
				project << patch::to_string(colors.val[0]) << endl;
			}
		}
		project.close();
		// ----------------------------------------------------------- //
		// --------------- End of computing 3D points ---------------- //
		// ----------------------------------------------------------- //

		// find maximum disparity for scaling output disparity images to [0..255]
		float disp_max = 0;
		for (int32_t i = 0; i < width*height; i++) {
			if (D1_data[i] > disp_max) disp_max = D1_data[i];
			if (D2_data[i] > disp_max) disp_max = D2_data[i];
		}

		// copy float to uchar
		image<uchar> *D1 = new image<uchar>(width, height);
		image<uchar> *D2 = new image<uchar>(width, height);
		for (int32_t i = 0; i < width*height; i++) {
			D1->data[i] = (uint8_t)max(255.0*D1_data[i] / disp_max, 0.0);
			D2->data[i] = (uint8_t)max(255.0*D2_data[i] / disp_max, 0.0);
		}

		// save disparity images
		char output_1[1024];
		char output_2[1024];
		strncpy(output_1, left_pgm.c_str(), strlen(left_pgm.c_str()) - 4);
		strncpy(output_2, right_pgm.c_str(), strlen(right_pgm.c_str()) - 4);
		output_1[strlen(left_pgm.c_str()) - 4] = '\0';
		output_2[strlen(right_pgm.c_str()) - 4] = '\0';
		strcat(output_1, "_disp.pgm");
		strcat(output_2, "_disp.pgm");
		savePGM(D1, output_1);
		savePGM(D2, output_2);

		// free memory
		delete I1;
		delete I2;
		delete D1;
		delete D2;
		free(D1_data);
		free(D2_data);
	}
}

int main(int argc, char** argv) {
	if (argc == 2 && !strcmp(argv[1], "demo")) {
		/*process("img/cones_left.pgm", "img/cones_right.pgm");
		process("img/aloe_left.pgm", "img/aloe_right.pgm");
		process("img/raindeer_left.pgm", "img/raindeer_right.pgm");
		process("img/urban1_left.pgm", "img/urban1_right.pgm");
		process("img/urban2_left.pgm", "img/urban2_right.pgm");
		process("img/urban3_left.pgm", "img/urban3_right.pgm");
		process("img/urban4_left.pgm", "img/urban4_right.pgm");*/
		cout << "... done!" << endl;
	}
	else if (argc == 5) {
		process(argv[1], argv[2], argv[3], argv[4]);
		cout << "... done!" << endl;
	}
	else {
		cout << endl;
		cout << "ELAS demo program usage: " << endl;
		cout << "./elas demo ................ process all test images (image dir)" << endl;
		cout << "./elas left.pgm right.pgm .. process a single stereo pair" << endl;
		cout << "./elas -h .................. shows this help" << endl;
		cout << endl;
		cout << "Note: All images must be pgm greylevel images. All output" << endl;
		cout << "      disparities will be scaled such that disp_max = 255." << endl;
		cout << endl;
	}
	return 0;
}


